package stockbit.bootcamp.base;

import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.github.cdimascio.dotenv.Dotenv;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import stockbit.bootcamp.utils.Constants;
import stockbit.bootcamp.utils.Utils;
import stockbit.test.android_driver.AndroidDriverInstance;

import java.util.NoSuchElementException;
import java.util.Objects;

public class BasePageObject {

    Dotenv env = Dotenv.load();

    public AndroidDriver driver() {
        return AndroidDriverInstance.androidDriver;
    }

    public AndroidElement waitStrategy(ExpectedCondition<WebElement> coditions) {
        WebDriverWait wait = new WebDriverWait(driver(), Constants.TIMEOUT);
        return (AndroidElement) wait.until(coditions);
    }

    public AndroidElement waitUntilClickable(By element) {
        return waitStrategy(ExpectedConditions.elementToBeClickable(element));
    }

    public AndroidElement waitUntilVisible(By element) {
        return waitStrategy(ExpectedConditions.visibilityOfElementLocated(element));
    }

    public By element(String elementLocator) {
        String elementValue = Utils.ELEMENTS.getProperty(elementLocator);
        if (elementValue == null) {
            throw new NoSuchElementException("Couldn't fine element : " + elementLocator);
        } else {
            String[] locator = elementValue.split("_");
            String locatorType = locator[0];
            String locatorValue = elementValue.substring(elementValue.indexOf("_") + 1);
            switch (locatorType) {
                case "id":
                    if (Objects.equals(env.get("ENV"), "prod")) {
                        return By.id("com.stockbit.android:id/" + locatorValue);
                    } else {
                        return By.id("com.stockbitdev.android:id/" + locatorValue);
                    }
                case "xpath":
                    return By.xpath(locatorValue);
                case "accessibilityId":
                    return MobileBy.AccessibilityId(elementLocator);
                default:
                    throw new IllegalStateException("Unexpected value: " + locatorType);
            }
        }
    }

    public void inputText(String locator, String text) {
        waitUntilVisible(element(locator)).sendKeys(text);
    }

    public void tap(String locator) {
        waitUntilClickable(element(locator)).click();
    }

    public void isDisplayed(String locator) {
        By element = element(locator);
        try {
            waitUntilVisible(element);
        } catch (TimeoutException e) {
            Utils.printError(String.format("Element [%s] not found!",element));
        }
        waitUntilVisible(element(locator)).isDisplayed();
    }

    public void isTextSame(String locator, String expectedText) {
        By element = element(locator);
        String actualText = waitUntilVisible(element).getText();
        if (!actualText.equals(expectedText)) {
            Utils.printError(String.format("Expected string [%s] is not match with actual string [%s]", element, actualText, expectedText));
        }
    }
}
