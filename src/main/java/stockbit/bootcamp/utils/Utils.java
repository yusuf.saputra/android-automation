package stockbit.bootcamp.utils;

import javax.swing.text.html.parser.Element;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.Properties;

public class Utils {

    public static Properties ELEMENTS;

    public static void loadElementProperties(String directory) {
        File folder = new File(directory);
        File[] listOfFiles = folder.listFiles();
        ELEMENTS = new Properties();

        for (int i = 0; i < Objects.requireNonNull(listOfFiles).length; ++i) {
            if ((listOfFiles[i].isFile() && listOfFiles[i].toString().contains(".properties"))) {
                try {
                    ELEMENTS.load(Files.newInputStream(Paths.get(directory + listOfFiles[i].getName())));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void printError(String errorMessage) {
        throw new AssertionError(errorMessage);
    }
}
