package stockbit.bootcamp.utils;

public class Constants {
    public static final String SRC_TEST_RESOURCES = System.getProperty("user.dir") + "/src/test/resources";
    public static String ELEMENTS = SRC_TEST_RESOURCES + "/elements/";
    public static Integer TIMEOUT = 15;
}
