package stockbit.bootcamp.page_object;

import kotlin.collections.ByteIterator;
import org.openqa.selenium.By;
import stockbit.bootcamp.base.BasePageObject;

public class LoginPage extends BasePageObject {

    public void inputUsername(String username) {
        inputText("FIELD_EMAIL_OR_USERNAME", username);
    }

    public void inputPassword(String password) {
        inputText("FIELD_PASSWORD", password);
    }

    public void ClickLoginOnBoarding() {
        tap("BUTTON_MASUK");
    }

    public void ClickMasuk() {
        tap("BUTTON_LOGIN");
    }

    public void SkipSmartlogin() {
        tap("BUTTON_SKIP_SMARTLOGIN_SOCIAL_POPUP");
    }

    public void Skip3dAvatar() {
        tap("BUTTON_SKIP_3D_AVATAR_POPUP");
    }

    public void watchlistPageValidation() {
        isDisplayed("LOGO_STOCKBIT");
    }

    public void isAlert(String alert) {
        isTextSame("ALERT_LOGIN", alert);
    }
}
