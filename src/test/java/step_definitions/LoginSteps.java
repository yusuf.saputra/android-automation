package step_definitions;

import io.cucumber.java8.En;
import stockbit.bootcamp.page_object.LoginPage;

public class LoginSteps implements En {
    private LoginPage loginPage;

    public LoginSteps() {
        loginPage = new LoginPage();

        Given("^User click Masuk in Onboarding Page$", () -> {
            loginPage.ClickLoginOnBoarding();
        });
        When("^User input username \"([^\"]*)\"$", (String username) -> {
            loginPage.inputUsername(username);
        });
        And("^User input password \"([^\"]*)\"$", (String password) -> {
            loginPage.inputPassword(password);
        });
        And("^User click Masuk$", () -> {
            loginPage.ClickMasuk();
        });
        Then("^User will direct to Watchlist Page$", () -> {
            loginPage.watchlistPageValidation();
        });
        And("^User click lewati smartlogin$", () -> {
            loginPage.SkipSmartlogin();
        });
        And("^User click lewati Avatar$", () -> {
            loginPage.Skip3dAvatar();
        });
        Then("^User will see error message \"([^\"]*)\"$", (String alert) -> {
            loginPage.isAlert(alert);
        });
    }
}
