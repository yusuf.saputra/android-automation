@Android
Feature: Login Social features

  @Android
  Scenario: Success Login Stockbit Social with Username
    Given User click Masuk in Onboarding Page
    When User input username "yaudah2"
    And User input password "stockbit"
    And User click Masuk
    And User click lewati smartlogin
    And User click lewati Avatar
    Then User will direct to Watchlist Page

  @Android
  Scenario: Success Login Stockbit Social with Email
    Given User click Masuk in Onboarding Page
    When User input username "yaudah2@mailsac.com"
    And User input password "stockbit"
    And User click Masuk
    And User click lewati smartlogin
    And User click lewati Avatar
    Then User will direct to Watchlist Page

#Negative Cases Scenario
  Scenario: Login with Invalid Username
    Given User click Masuk in Onboarding Page
    When User input username "INVALID_USERNAME"
    And User input password "stockbit"
    And User click Masuk
    Then User will see error message "Username atau password salah. Mohon coba lagi."

  Scenario: Login with Invalid Password
    Given User click Masuk in Onboarding Page
    When User input username "yaudah2"
    And User input password "INVALID_PASSWORD"
    And User click Masuk
    Then User will see error message "Username atau password salah. Mohon coba lagi."

  Scenario: Login with Blank Username and Password
    Given User click Masuk in Onboarding Page
    When User input username ""
    And User input password ""
    And User click Masuk
    Then User will see error message "Mohon masukkan username/email dan password kamu."
