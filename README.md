# Automation Test Setup Guide

This guide provides instructions for setting up the necessary dependencies and tools required to run automation tests using JDK and Appium. It includes information on the dependencies used and step-by-step instructions for configuring JDK and Appium.

## Table of Contents

1. [API Dependencies](#API-dependencies)
2. [JDK Installation](#jdk-installation)
3. [Appium Setup](#appium-setup)
4. [Running Automation Tests](#running-automation-tests)
5. [Troubleshooting](#troubleshooting)
6. [Contributing](#contributing)
7. [License](#license)

## API Dependencies

Here is an explanation of the required API dependencies listed in the given block:

1. `org.junit.jupiter:junit-jupiter:5.8.2`
    - Description: This dependency is for the JUnit Jupiter testing framework, which provides annotations and APIs for writing and executing tests.
    - Version: 5.8.2
    - Usage: Used for writing and executing tests using JUnit Jupiter.

2. `junit:junit:4.13.2`
    - Description: This dependency is for JUnit, a popular testing framework for Java applications.
    - Version: 4.13.2
    - Usage: Used for writing and executing tests using JUnit.

3. `org.junit.jupiter:junit-jupiter-engine:5.8.1`
    - Description: This dependency provides the JUnit Jupiter test engine, which is responsible for discovering and executing JUnit Jupiter tests.
    - Version: 5.8.1
    - Usage: Used for test execution with JUnit Jupiter.

4. `org.junit.vintage:junit-vintage-engine:5.8.2`
    - Description: This dependency provides the JUnit Vintage test engine, which allows you to run JUnit 3 and JUnit 4 tests on the JUnit Platform.
    - Version: 5.8.2
    - Usage: Used for running older JUnit 3 and JUnit 4 tests on the JUnit Platform.

5. `org.junit.jupiter:junit-jupiter-api:5.8.2`
    - Description: This dependency contains the JUnit Jupiter API, which provides the programming model and annotations for writing JUnit Jupiter tests.
    - Version: 5.8.2
    - Usage: Used for writing JUnit Jupiter tests.

6. `org.seleniumhq.selenium:selenium-java:3.141.59`
    - Description: This dependency provides the Selenium WebDriver API, which allows you to automate web browsers for testing purposes.
    - Version: 3.141.59
    - Usage: Used for web automation and browser testing with Selenium WebDriver.

7. `io.cucumber:cucumber-java:7.12.0`
    - Description: This dependency is for Cucumber-JVM, a tool for behavior-driven development (BDD) that allows you to write and execute feature files and step definitions.
    - Version: 7.12.0
    - Usage: Used for writing and executing Cucumber feature files and step definitions using Java.

8. `io.cucumber:cucumber-junit:7.12.0`
    - Description: This dependency allows you to run Cucumber tests as JUnit tests.
    - Version: 7.12.0
    - Usage: Used for running Cucumber tests as JUnit tests.

9. `io.appium:java-client:7.5.1`
    - Description: This dependency provides the Java client library for the Appium mobile automation framework, allowing you to write automation tests for mobile apps.
    - Version: 7.5.1
    - Usage: Used for mobile automation testing with Appium.

10. `io.github.cdimascio:java-dotenv:5.2.2`
    - Description: This dependency allows you to read configuration variables from a .env file in your Java application.
    - Version: 5.2.2
    - Usage: Used for managing environment variables and configurations with a .env file.

11. `org.apache.logging.log4j:log4j-core:2.19.0`
    - Description: This dependency provides the Log4j logging library, which allows you to log messages and events in your application.


- Version: 2.19.0
    - Usage: Used for logging purposes in the application.

Make sure to include these dependencies in your project's build configuration file (e.g., build.gradle or pom.xml) according to the syntax and conventions used by your build tool (e.g., Gradle or Maven).



## JDK Installation

To install the JDK and configure it for running automation tests, follow these steps:

1. Visit the [Oracle JDK](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html) website.

2. Download the appropriate JDK distribution for your operating system.

3. Follow the installation instructions provided by Oracle for your specific operating system.

4. Set the `JAVA_HOME` environment variable:
    - **Windows**: Go to System Properties → Advanced system settings → Environment Variables. Add a new system variable named `JAVA_HOME` and set its value to the JDK installation directory (e.g., `C:\Program Files\Java\jdk1.8.0_291`).
    - **macOS/Linux**: Open the terminal and execute the following command, replacing `<path_to_jdk>` with the JDK installation directory:
      ```bash
      export JAVA_HOME=<path_to_jdk>
      ```

5. Add the JDK's `bin` directory to the `PATH` environment variable:
    - **Windows**: Edit the `Path` system variable and append `;%JAVA_HOME%\bin;` at the end.
    - **macOS/Linux**: Open the terminal and execute the following command:
      ```bash
      export PATH=$PATH:$JAVA_HOME/bin
      ```

6. Verify the JDK installation by running the following command in the terminal:
   ```bash
   java -version
   ```
   You should see the JDK version information displayed.

## Appium Setup

To set up Appium for running automation tests, follow these steps:

1. Install Node.js:
    - Visit the [Node.js](https://nodejs.org/) website.
    - Download and run the installer for your operating system.
    - Follow the installation instructions provided by Node.js.

2. Install Appium:
    - Open the terminal or command prompt.
    - Execute the following command to install Appium globally:
      ```bash
      npm install -g appium
      ```

3. Install Appium server dependencies:
    - Execute the following command to install the necessary dependencies for running the Appium server:
      ```bash
      appium doctor
      ```
    - Follow the instructions provided by the Appium Doctor to install any missing dependencies.

4. Start the Appium server:
    - Open a new terminal or command prompt window.
    - Execute the following command to start the Appium server:
      ```bash
      appium
      ```

5. Verify the Appium installation by checking the server status. Open a web browser and visit `http://localhost:4723/wd/hub/status`. If Appium is running correctly, you should see status information displayed in JSON format.

## Running Automation Tests

To execute the automation tests using JDK and Appium, follow these steps:

1. Clone the repository containing the automation tests.

2. Install the required dependencies by adding them to your project's build configuration file (e.g., build.gradle or pom.xml) according to the syntax and conventions used by your build tool (e.g., Gradle or Maven).

3. Configure the test scripts or test framework with the necessary capabilities, desired capabilities, and device configurations as per your requirements.

4. Run the automation tests using button play in features file

## Troubleshooting

If you encounter any issues during the setup or execution of the automation tests, consider the following troubleshooting steps:

1. Make sure all the dependencies, including JDK and Appium, are installed correctly.

2. Double-check the configuration settings for JDK and Appium, such as environment variables and PATH.

3. Check for any error messages or exceptions in the console output during the setup or test execution.

4. Refer to the official documentation and troubleshooting guides for JDK and Appium for known issues and solutions.

5. If the issue persists, consider searching the project's issue tracker or community forums for similar problems and solutions.

6. If no solution is found, consider opening a new issue in the project's repository, providing detailed information about the problem you're facing.

## Contributing

Contributions to the automation tests are welcome! If you have any suggestions, improvements, or bug fixes, please submit a pull request. Make sure to follow the project's guidelines for contributing.

## License

This automation test framework is licensed under the [MIT License](LICENSE). Feel free to use, modify, and distribute it as per the terms of the license.